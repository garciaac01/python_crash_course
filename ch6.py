p1 = {"name":"Andy", "age":29, "city":"Indianapolis"}
p2 = {"name":"Caitlin", "age":29, "city": "Glenview"}
p3 = {"name":"Gizmo", "age":7, "city": "Fort Wayne"}
people = [p1, p2, p3]

for person in people:
    print("%s, %d, %s" % (person['name'], person['age'], person['city']))
