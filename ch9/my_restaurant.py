from restaurant import Restaurant

# my_ice_cream_stand = IceCreamStand("Gizzy's ice cream", "dessert", ['chocolate', 'cookies and cream'])
# print my_ice_cream_stand.flavors

# class IceCreamStand(Restaurant):
#     def __init__(self, name, cuisine, flavors=['chocolate', 'strawberry', 'vanilla']):
#         super(IceCreamStand, self).__init__(name, cuisine)
#         self.flavors = flavors
my_restaurant = Restaurant("Gizzy's", "bar food")
my_restaurant.describe_restaurant()
my_restaurant.open_restaurant()
my_restaurant.set_number_served(35)
my_restaurant.increment_number_served(10)
print("We've served %d customers" % my_restaurant.number_served)

print my_restaurant.cuisine_type



class User(object):
    def __init__(self, name, age, phone):
        self.name = name
        self.age = age
        self.phone = phone
    
    def describe_user(self):
        print "Name: %s\nAge: %s\nPhone: %s" % (self.name, self.age, self.phone)


# new_user = User("Andy", 33, "260-555-1111")
# new_user.describe_user()
