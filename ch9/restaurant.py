"""Practice class"""
class Restaurant(object):
    """Restaurant object"""
    def __init__(self, name, cuisine):
        self.restaurant_name = name
        self.cuisine_type = cuisine
        self.number_served = 0

    def describe_restaurant(self):
        """print out the restaurant's details"""
        print "%s is a %s restaurant." %(self.restaurant_name, self.cuisine_type)

    def open_restaurant(self):
        """open the restaurant"""
        print "%s is now open" % self.restaurant_name
    
    def set_number_served(self, served):
        self.number_served = served
    
    def increment_number_served(self, served):
        self.number_served += served