"""This is for actually making the pizza"""

from pizza import make_pizza as mp

mp(16, "pepperoni")
mp(12, 'mushrooms', 'green peppers')

def long_function(parameter1, parameter2, parameter3, parameter4, parameter5):
    """my docstring"""
    print "does this work?"

long_function(1, 2, 3, 4, 5)
