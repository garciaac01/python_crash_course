import json

def favorite_number():
    favorite = get_number()

    if favorite:
        print("I remember your favorite! It's %s" % favorite)
    else:
        favorite = get_new_number()
        print("Okay, next time I'll remember that your favorite number is %s" % favorite)

def get_number():
    try:
        with open("favorite_num.json") as file_object:
            favorite = json.load(file_object)
    except IOError:
        return None
    else:
        return favorite

def get_new_number():
    new_favorite = raw_input("Enter your favorite number: ")
    with open("favorite_num.json", 'w') as file_object:
        json.dump(new_favorite, file_object)
    return new_favorite

favorite_number()