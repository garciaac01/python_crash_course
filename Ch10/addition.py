"""Add two numbers with error handling"""
def addition():
    """Add two numbers with error handling"""
    while True:
        first_number = raw_input("Enter the first number: ")
        second_number = raw_input("Enter the second number: ")

        try:
            total = int(first_number) + int(second_number)
        except ValueError:
            print("\nInvalid input\n")
        else:
            print("The sum is %d" % total)
            break

addition()
