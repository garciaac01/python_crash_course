"""Example of using a unit test"""
def city_country(city, country, population):
    """Returns a formatted city and country name"""
    return "%s, %s - population %s" % (city, country, str(population))
