"""Class used for class testing"""
class Employee():
    """Employee class that we will test"""

    def __init__(self, first_name, last_name, salary):
        """Constructor to initialize the fields"""
        self.first_name = first_name
        self.last_name = last_name
        self.salary = salary

    def give_raise(self, amount=5000):
        """Adds 5k to salary"""
        self.salary += amount
