"""Unit test for the employee module"""
import unittest
from employee import Employee

class TestEmployee(unittest.TestCase):
    """Unit test"""
    def setUp(self):
        """Create an instance of employee"""
        self.my_employee = Employee("Jim", "Bob", 50000)

    def test_default_raise(self):
        """test the default raise"""
        self.my_employee.give_raise()
        self.assertEqual(self.my_employee.salary, 55000)

    def test_custom_raise(self):
        """test custom raise"""
        self.my_employee.give_raise(10000)
        self.assertEqual(self.my_employee.salary, 60000)

unittest.main()
