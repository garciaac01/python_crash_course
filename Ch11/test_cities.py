"""Unit test of city_functions.py file"""
import unittest
from city_functions import city_country

class CityUnitTest(unittest.TestCase):
    """Tests for city_functions.py"""

    def test_first_city_country(self):
        """Test Cancun, Mexico"""
        formatted_city = city_country("Cancun", "Mexico", 5000)
        self.assertEqual(formatted_city, "Cancun, Mexico - population 5000")

    def test_second_city_country(self):
        """Test, Chicago, IL"""
        formatted_city = city_country("Chicago", "Illinois", 1000000)
        self.assertEqual(formatted_city, "Chicago, Illinois - population 1000000")

unittest.main()
